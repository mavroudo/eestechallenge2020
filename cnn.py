#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar  7 20:59:05 2020

@author: mavroudo
"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


from keras.models import Sequential
from keras.layers import Convolution2D,MaxPooling2D,Flatten,Dense

classifier = Sequential()

# Step 1 - Convolution
classifier.add(Convolution2D(filters=32,kernel_size=(3,3),input_shape=(64,64,3),activation='relu'))

# Step 2 - Max pooling, reduce the dimentionality
classifier.add(MaxPooling2D(pool_size=(2,2))) #normally used and you dont loose a lot

# Step 3 - flattening
classifier.add(Flatten())

# Step 4 - Full connection
classifier.add(Dense(128,activation='relu'))
classifier.add(Dense(1,activation='sigmoid')) # for boolean or something other for more

#Compiling
classifier.compile(optimizer='adam',loss='binary_crossentropy',metrics=['accuracy']) #categorical ctossentropy if more

