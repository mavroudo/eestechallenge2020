#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  8 09:35:22 2020

@author: mavroudo
"""

from gtts import gTTS
import os

#Joke
tts = gTTS(text='As we waiting for speach recognision, allow me to tell you a joke. How do you call a camel with three humps? Pregnant! Hahaha', lang='en')
tts.save("commands/joke.mp3")
os.system("mpg321 commands/joke.mp3")


#For log in
tts = gTTS(text='You want to log in. You can say "back" at any point and you will be redirected to home page', lang='en')
tts.save("commands/logInStarting.mp3")
os.system("mpg321 commands/logIn1.mp3")

tts = gTTS(text='Now tell me your password', lang='en')
tts.save("commands/password.mp3")
os.system("mpg321 commands/password.mp3")

tts = gTTS(text='Please tell me your username.', lang='en')
tts.save("commands/username.mp3")
os.system("mpg321 commands/username.mp3")

tts = gTTS(text='You have succesfully logged in! You are now in the Dashboard', lang='en')
tts.save("commands/logInSuccess.mp3")
os.system("mpg321 commands/logInSuccess.mp3")

tts = gTTS(text='Your credentials were incorrect. Do you want to try again?', lang='en')
tts.save("commands/logInFail.mp3")
os.system("mpg321 commands/logInFail.mp3")

tts = gTTS(text='You have been redirected to home page.', lang='en')
tts.save("commands/backToHomePage.mp3")
os.system("mpg321 commands/backToHomePage.mp3")


#For log out
tts = gTTS(text='Are you sure you want to log out?', lang='en')
tts.save("commands/logOut.mp3")
os.system("mpg321 commands/Logout.mp3")

tts = gTTS(text='You have successfully logged out', lang='en')
tts.save("commands/logOutSuccess.mp3")
os.system("mpg321 commands/LogoutSuccess.mp3")

#For register
tts = gTTS(text='You want to register. You can say "back" at any point and you will be redirected to home page', lang='en')
tts.save("commands/registerStarting.mp3")
os.system("mpg321 commands/registerStarting.mp3")

tts = gTTS(text='This username is used. Please tell me a new one.', lang='en')
tts.save("commands/usernameUsed.mp3")
os.system("mpg321 commands/usernameUsed.mp3")

tts = gTTS(text='Your account has been successfullyCreated! You are now in the Dashboard.', lang='en')
tts.save("commands/successfullyCreated.mp3")
os.system("mpg321 commands/successfullyCreated.mp3")

#testing audio
from os import path
from pydub import AudioSegment
tts = gTTS(text='capital a b c d capital e 1 2 3', lang='en')
tts.save("testingAudio/password.mp3")
os.system("mpg321 testingAudio/password.mp3")
sound = AudioSegment.from_mp3('testingAudio/password.mp3')
sound.export('testingAudio/password.wav', format="wav")
