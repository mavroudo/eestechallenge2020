#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  8 08:43:39 2020

@author: mavroudo
"""

import speech_recognition as sr

r = sr.Recognizer()
mic=sr.Microphone(device_index=6)

#sr.Microphone.list_microphone_names()
#audio=None
#with mic as source:
#    audio=r.listen(source)
    
    
#file = sr.AudioFile('OSR_us_000_0010_8k.wav') #this is an example



def getTextFromAudio(file):
    fileRead = sr.AudioFile(file) 
    with fileRead as source:
        r.adjust_for_ambient_noise(source)
        audio = r.record(source)
    return r.recognize_google(audio,language = 'en-GB', show_all=False)


def logIn():
    """
    This will define the process in the of looging in
     1) play audio logInStarting
     2) play audio username
     3) get username from speach recognition
     4) play audio password
     5) get password from speach recognition
     6) if credentials correnct:
            play audio logInSuccess
        else:
            play audio logInFail
            if they want to try again:
                go to step 2
            else:
                play audio backToHomePage
    """
    pass

def logOut():
    """
        This will define the process in the log out
        1) play audio logOut
            if yes:
                play audio logOutSuccess
    """
    pass

def register():
    """
    This will define the process of log out:
    1) play audio registerStarting
    2) play audio username
    3) if username not in database:
            play audio password
        else:
            play audio usernameUsed
    4) play audio successfullyCreated
    """
    pass

def back():
    """
    This will define the process of saying "back":
    1) play audio backToHomePage
    """
    pass

    
      