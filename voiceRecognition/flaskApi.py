#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  8 10:50:45 2020

@author: mavroudo
"""
import base64
import os
import wave
from io import BytesIO

import flask
from PIL import Image
from flask import request, jsonify

import voiceRecognition
from face_rec.face_rec import classify_face

app = flask.Flask(__name__)
app.config["DEBUG"] = True
APP_ROOT = os.path.dirname(os.path.abspath(__file__))
print(APP_ROOT)
UPLOAD_FOLDER = os.path.join(APP_ROOT, '../', 'face_rec', 'faces')
print(UPLOAD_FOLDER)


@app.route('/', methods=['GET'])
def home():
    return "<h1>Distant Reading Archive</h1><p>This site is a prototype API for distant reading of science fiction novels.</p>"


@app.route('/voice_request/file', methods=['POST'])
def getVoice():
    print(request.json)
    filePosition = request.json.get("fileName")
    number = request.json.get("number")
    print(filePosition, number)
    return jsonify(test=voiceRecognition.getTextFromAudio(filePosition))


@app.route('/create-user', methods=['POST'])
def createUser():
    print(request.json)

    image = base64.decodebytes(str.encode(request.json.get("capture").split(",")[1]))

    stream = BytesIO(image)
    image = Image.open(stream).convert("RGBA")
    stream.close()
    image.save(os.path.join(UPLOAD_FOLDER, "{}".format(request.json.get("username") + ".png")))

    return {}


@app.route('/milaw', methods=['POST'])
def milaw():
    sound = base64.decodebytes(str.encode(request.json.get("audio").split(",")[1]))
    obj = wave.open(os.path.join(APP_ROOT, "filesListen/{}".format('sound.wav')), 'wb')
    obj.setnchannels(1)  # mono
    obj.setsampwidth(2)
    obj.setframerate(44100)
    obj.writeframes(sound)

    return {}


@app.route('/validate-image', methods=['POST'])
def getValidationWithImage():
    # get
    print(request.json)
    image = base64.decodebytes(str.encode(request.json.get("image").split(",")[1]))
    print(image)
    stream = BytesIO(image)
    image = Image.open(stream).convert("RGBA")
    stream.close()
    image.save(os.path.join(UPLOAD_FOLDER, "../", "{}".format("test1.png")))
    # run face recognition
    path=os.path.join(APP_ROOT,"../face_rec", "{}".format("test1.png"))
    pathToImages=os.path.join(APP_ROOT,"../face_rec/faces")
    print(path)
    data=classify_face(path,pathToImages)
    print(data)
    if len(data)==0 or data[0]=='Unknown':
        return jsonify(found=False)
    else:
        return jsonify(found=True,username=data[0])

app.run()
