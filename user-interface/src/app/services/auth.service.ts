import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(private http: HttpClient) {
    }

    register(requestData: any): void {
        this.http.post<null>('http://localhost:8080/http://localhost:5000/create-user', requestData).subscribe(data => {
            console.log(data);
        });
    }

    milaw(requestData: any): void {
        this.http.post<null>('http://localhost:8080/http://localhost:5000/milaw', requestData).subscribe(data => {
            console.log(data);
        });
    }

    validateImage(requestData: any): any {
        return this.http.post<null>('http://localhost:8080/http://localhost:5000/validate-image', requestData);
    }

    logout(): void {
        localStorage.setItem('isLoggedIn', 'false');
        localStorage.removeItem('token');
    }
}
