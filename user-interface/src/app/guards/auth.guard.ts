import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const requiresLogin = route.data.requiresLogin || false;
        const requiresLogout = route.data.requiresLogout || false;

        if (requiresLogin) {
            if (this.isLoggedIn()) {
                return true;
            }

            this.router.navigate(['/home']);
            return false;
        } else if (requiresLogout) {
            if (!this.isLoggedIn()) {
                return true;
            }

            this.router.navigate(['/dashboard']);
            return false;
        }
    }

    public isLoggedIn(): boolean {
        return localStorage.getItem('isLoggedIn') === 'true';
    }
}
