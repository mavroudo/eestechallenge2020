import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
    selector: 'app-login-wizard',
    templateUrl: './login-wizard.component.html',
    styleUrls: ['./login-wizard.component.scss']
})
export class LoginWizardComponent implements OnInit, AfterViewInit {
    firstFormGroup: FormGroup;
    secondFormGroup: FormGroup;
    matcher = new MyErrorStateMatcher();

    @ViewChild('video')
    public video: ElementRef;

    @ViewChild('canvas')
    public canvas: ElementRef;

    public captures: any;

    constructor(private formBuilder: FormBuilder, private service: AuthService, private router: Router) {
        this.captures = undefined;
    }

    ngOnInit() {
        this.firstFormGroup = this.formBuilder.group({
            username: ['', Validators.required]
        });
        this.secondFormGroup = this.formBuilder.group({
            password: ['', Validators.required],
            passwordConfirm: ['', Validators.required]
        }, {validator: this.checkPasswords});
    }

    public ngAfterViewInit() {
        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            navigator.mediaDevices.getUserMedia({video: true}).then(stream => {
                this.video.nativeElement.srcObject = stream;
                this.video.nativeElement.play();
            });
        }
    }

    checkPasswords(group: FormGroup) { // here we have the 'passwords' group
        const pass = group.get('password').value;
        const confirmPass = group.get('passwordConfirm').value;

        return pass === confirmPass ? null : {notSame: true};
    }

    public capture() {
        const context = this.canvas.nativeElement.getContext('2d').drawImage(this.video.nativeElement, 0, 0, 640, 480);
        this.captures = this.canvas.nativeElement.toDataURL('image/png');
    }

    public save() {
        this.service.register({
            username: this.firstFormGroup.controls.username.value,
            password: this.secondFormGroup.controls.password.value,
            capture: this.captures
        });
        localStorage.setItem('isLoggedIn', 'true');
        localStorage.setItem('token', this.firstFormGroup.controls.username.value);
        this.router.navigate(['/dashboard']);
    }
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
        const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);

        return (invalidCtrl || invalidParent);
    }
}
