import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActualLoginComponent } from './actual-login/actual-login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from './guards/auth.guard';
import { HomePageComponent } from './home-page/home-page.component';
import { LoginWizardComponent } from './login-wizard/login-wizard.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RegisterWizardComponent } from './register-wizard/register-wizard.component';

const routes: Routes = [
    {path: 'dashboard', component: DashboardComponent, data: {requiresLogin: true}, canActivate: [AuthGuard]},
    {path: 'home', component: HomePageComponent},
    {path: 'voice', component: RegisterWizardComponent},
    {path: 'login', component: ActualLoginComponent, data: {requiresLogout: true}, canActivate: [AuthGuard]},
    {path: 'register', component: LoginWizardComponent, data: {requiresLogout: true}, canActivate: [AuthGuard]},
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
    },
    {path: '**', component: PageNotFoundComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
