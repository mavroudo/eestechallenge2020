import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AudioRecordingService } from '../services/audio-recording.service';
import { AuthService } from '../services/auth.service';

@Component({
    selector: 'app-register-wizard',
    templateUrl: './register-wizard.component.html',
    styleUrls: ['./register-wizard.component.scss']
})
export class RegisterWizardComponent implements OnInit {

    isRecording = false;
    recordedTime;
    blobEncoded;

    constructor(private audioRecordingService: AudioRecordingService, private sanitizer: DomSanitizer, private service: AuthService) {

        this.audioRecordingService.recordingFailedFun().subscribe(() => {
            this.isRecording = false;
        });

        this.audioRecordingService.getRecordedTime().subscribe((time) => {
            this.recordedTime = time;
        });

        this.audioRecordingService.getRecordedBlob().subscribe((data) => {
            const reader = new FileReader();
            reader.readAsDataURL(data.blob);
            reader.onloadend = () => {
                this.blobEncoded = reader.result;
                this.service.milaw({audio: this.blobEncoded});
                this.clearRecordedData();
            };
        });
    }

    startRecording() {
        if (!this.isRecording) {
            this.isRecording = true;
            this.audioRecordingService.startRecording();
        }
    }

    stopRecording() {
        if (this.isRecording) {
            this.audioRecordingService.stopRecording();
            this.isRecording = false;
        }
    }

    clearRecordedData() {
        this.blobEncoded = null;
    }

    ngOnInit(): void {
    }

}
