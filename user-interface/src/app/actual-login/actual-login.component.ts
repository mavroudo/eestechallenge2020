import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
    selector: 'app-actual-login',
    templateUrl: './actual-login.component.html',
    styleUrls: ['./actual-login.component.scss']
})
export class ActualLoginComponent implements OnInit, AfterViewInit {
    firstFormGroup: FormGroup;
    loginWay = true;

    @ViewChild('video')
    public video: ElementRef;

    @ViewChild('canvas')
    public canvas: ElementRef;

    public captures: any;

    constructor(private formBuilder: FormBuilder, private service: AuthService, private router: Router) {
        this.captures = undefined;
    }

    ngOnInit() {
        this.firstFormGroup = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required],
        });
    }

    public ngAfterViewInit() {
        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            navigator.mediaDevices.getUserMedia({video: true}).then(stream => {
                this.video.nativeElement.srcObject = stream;
                this.video.nativeElement.play();
            });
        }
    }

    public capture() {
        const context = this.canvas.nativeElement.getContext('2d').drawImage(this.video.nativeElement, 0, 0, 640, 480);
        this.captures = this.canvas.nativeElement.toDataURL('image/png');
        this.service.validateImage({image: this.captures}).subscribe(data => {
            if (data.found) {
                localStorage.setItem('isLoggedIn', 'true');
                localStorage.setItem('token', data.username);
                this.router.navigate(['/dashboard']);
            }
            this.captures = undefined;
            console.log(data);
            return data;
        });

    }

    public login() {
        localStorage.setItem('isLoggedIn', 'true');
        localStorage.setItem('token', this.firstFormGroup.controls.username.value);
        this.router.navigate(['/dashboard']);
    }
}
