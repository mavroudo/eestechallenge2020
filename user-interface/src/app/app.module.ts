import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatStepperModule } from '@angular/material/stepper';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActualLoginComponent } from './actual-login/actual-login.component';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from './guards/auth.guard';
import { HomePageComponent } from './home-page/home-page.component';
import { LoginWizardComponent } from './login-wizard/login-wizard.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RegisterWizardComponent } from './register-wizard/register-wizard.component';

@NgModule({
    declarations: [
        AppComponent,
        LoginWizardComponent,
        HomePageComponent,
        PageNotFoundComponent,
        DashboardComponent,
        RegisterWizardComponent,
        ActualLoginComponent
    ],
    imports: [
        MatInputModule,
        BrowserModule,
        AppRoutingModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MatStepperModule,
        MatFormFieldModule,
        MatButtonModule,
        HttpClientModule,
        MatSlideToggleModule
    ],
    providers: [AuthGuard],
    bootstrap: [AppComponent]
})
export class AppModule {
}
